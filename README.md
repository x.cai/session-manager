# session-manager
Creates the resources needed for the use of [AWS Systems Manager Session Manager](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager.html)

# Usage
* Deploy the stack with:
  ```
  git clone https://gitlab.com/aw5academy/terraform/session-manager.git
  cd session-manager
  terraform init
  terraform apply
  ```

# Post Apply Steps
Some things are not in Terraform yet and must be set manually. These are the Session Manager preferences. To set these:
- Login to the AWS console;
- Open the Systems Manager service;
- Click on 'Session Manager' under 'Instances & Nodes';
- Click on the 'Preferences' tab;
- Click 'Edit';
- Enable KMS Encryption and point to the `alias/session-manager` key;
- Enable session logging to S3 bucket `ssm-session-logs...` with encryption enabled;
- Enable session logging to CloudWatch log group `/aws/ssm/session-logs` with encryption enabled;
- Save the changes;

# Workstation Configuration
To be able to use Session Manager from the [AWS CLI](https://aws.amazon.com/cli/) you also need to install the [Session Manager Plugin](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html).

## Linux Install Instructions
```
curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/linux_64bit/session-manager-plugin.rpm" -o "session-manager-plugin.rpm"
sudo yum install -y session-manager-plugin.rpm
```

## Ubuntu Install Instructions
```
curl "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb" -o "session-manager-plugin.deb"
sudo dpkg -i session-manager-plugin.deb
```

## Install Verification
Verify the install by running:
```
session-manager-plugin
```
You should see the following output:
```
The Session Manager plugin is installed successfully. Use the AWS CLI to start a session.
```

# Testing
We can test that session manager is working by launching an EC2 instance into the private subnet with the `session-manager` instance profile. This instance can be launched by issuing the following command:
```
aws ec2 run-instances \
    --image-id $(aws ssm get-parameters --names /aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2 --query 'Parameters[0].[Value]' --output text --region us-east-1) \
    --instance-type t3a.nano \
    --subnet-id $(terraform output private-subnet-id) \
    --iam-instance-profile Name=session-manager \
    --output json \
    --region us-east-1 \
    --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=session-manager-test}]' \
    --count 1 > /tmp/ssm-test-instance.json
```

Issue this command to wait for the instance to become ready:
```
while true; do if [[ $(aws ssm describe-instance-information --filters "Key=InstanceIds,Values=`cat /tmp/ssm-test-instance.json |jq -r .Instances[0].InstanceId`" --region us-east-1 |jq -r .InstanceInformationList[0].PingStatus) == "Online" ]]; then echo "Instance ready." && break; else echo "Instance starting..." && sleep 5; fi; done
```

When ready, you should then be able to connect to the instance with session manager via:
```
aws ssm start-session --target $(cat /tmp/ssm-test-instance.json |jq -r .Instances[0].InstanceId) --region us-east-1
```

# Linux Run As
By default, session manager sessions are launched via a system-generated `ssm-user`. [This](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-preferences-run-as.html) article from AWS explains how you can change this so that sessions use another user account on the EC2 instance. Additionally, you may tag IAM roles or users with `SSMSessionRunAs` and a value being the user account to login with to override this account level setting.

# Cleanup
Cleanup this stack by running the following commands:
```
aws ec2 terminate-instances --instance-ids $(cat /tmp/ssm-test-instance.json |jq -r .Instances[0].InstanceId) --region us-east-1
rm -f /tmp/ssm-test-instance.json
terraform init
terraform destroy
```
