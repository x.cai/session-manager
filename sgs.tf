resource "aws_security_group" "endpoints" {
  description = "Security group for VPC endpoints"
  name        = "endpoints-sg"
  tags = {
    Name    = "endpoints-sg"
  }
  vpc_id = module.vpc.vpc-id
}

resource "aws_security_group_rule" "endpoints-https" {
  cidr_blocks = [
    module.vpc.private-subnet-cidr
  ]
  description       = "HTTPS access from private subnet"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.endpoints.id
  to_port           = 443
  type              = "ingress"
}
